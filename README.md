# mutantC-v4
# Checkout the poject website from [here](https://mutantc.gitlab.io).
### An ESP32 based casual smartwatch with 1.7 inch display, WiFi, Bletooth, NeoPixel and Vibration. It supports variety of watchbands.
### It is fully open-source hardware and software with MIT. So you can modify or hack it as you wish.
### So help us to make a Community around it.
### [Parts list](https://gitlab.com/mutantC/mutantw-v1/-/blob/main/parts_list).

### [Youtube](http://www.youtube.com/c/mutantC).

# Download
### Download the project from [here](https://a360.co/3kQQ7FA).

# Software used
### Autodesk fusion 360 - 3D parts
### Eagle - PCB
### Ardinuo IDE - Device Firmware

<img src="p1.JPG" width="500">
<img src="p2.JPG" width="500"> 
<img src="p3.JPG" width="500"> 
<img src="p4.JPG" width="500"> 
<img src="p5.JPG" width="500">
<img src="p6.JPG" width="500">

# Feedback
We need your feedback to improve the mutantW.
Send us your feedback through GitLab [issues](https://gitlab.com/groups/mutantC/-/issues).
