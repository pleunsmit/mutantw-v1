// firmware V1.5 for mutantW v1
// Install this libs before building the firmware
#include <Adafruit_ST7789.h> 
#include <SPI.h>
#include <WiFi.h>
#include <esp_now.h>
#include "time.h"

// Set the hotspot pass you will use to get the time from the internet
const char* ssid     = "DESKTOP";
const char* password = "84r19+H8";

// You can change the gmtOffset_sec to set your local time
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = -14400;
const int   daylightOffset_sec = 0;

// For LCD
#define TFT_CS         14
#define TFT_RST        15
#define TFT_DC         32

// For buttons and others
const int LITE = 33;
const int UP = 2;
const int DOWN = 16;
const int TOUCH = 5;
const int BATTERY_M = 34;
const int VIBRATION = 35;

// For LCD
Adafruit_ST7789 tft = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_RST);

// For buttons and others
int touchState = 1;    
static bool upState = 0;
static bool downState = 1;
static uint16_t powered_on = 10000;

// For Display turn off
unsigned long DELAY_TIME = 6000; // 6 sec
unsigned long delayStart = 0; // the time the delay started


static void notification_Vibration(void)
{
  Serial.println("notification_Buzzer");
  digitalWrite(VIBRATION, HIGH);
  delay(1000);
  digitalWrite(VIBRATION, LOW);
  delay(800);
  digitalWrite(VIBRATION, HIGH);
  delay(1000);
  digitalWrite(VIBRATION, LOW);
}

void watchFace() {
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }

  tft.setCursor(80, 30);
  tft.fillScreen(ST77XX_BLACK);
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(13.5);
  tft.println(&timeinfo, "%H");
  tft.setTextColor(ST77XX_YELLOW);
  tft.setTextSize(13.5);
  tft.println(&timeinfo, ":%M");
}

void secondFace() {
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }

  // Get Battery voltage
  uint32_t value = analogRead(BATTERY_M);
  Serial.print("Voltage= ");
  Serial.println(value);

  tft.setCursor(0, 30);
  tft.fillScreen(ST77XX_BLACK);
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(2);
  tft.println(&timeinfo, "%A");
  tft.setTextColor(ST77XX_YELLOW);
  tft.setTextSize(2);
  tft.println(&timeinfo, "%B");
  tft.setTextColor(ST77XX_GREEN);
  tft.setTextSize(3);
  tft.println(&timeinfo, "%d");
  tft.setTextColor(ST77XX_WHITE);
  tft.setTextSize(2);
  tft.println(&timeinfo, "%Y");
  tft.setTextSize(3);
  tft.setTextColor(ST77XX_GREEN);
  tft.setTextSize(5);
  tft.println(&timeinfo, "%H:%M:%S");
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(4);
  tft.println("Voltage= ");
  tft.println(value);
}

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message {
  char a[32];
  char b[32];
} struct_message;

// Create a struct_message called myData
struct_message myData;

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("Char: ");
  Serial.println(myData.a);
  Serial.print("Char: ");
  Serial.println(myData.b);
  
  delayStart = millis();
  turnDisplayON();
  notificationFace();
//  notification_Vibration();
  downState =1;
}

void notificationFace() {
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  
  tft.setCursor(10, 30);
  tft.fillScreen(ST77XX_BLACK);
  
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(2);
  tft.println(&timeinfo, "%H:%M:%S");
  
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(2);
  tft.println( myData.a);
  
  tft.setTextColor(ST77XX_YELLOW);
  tft.setTextSize(2);
  tft.println( myData.b);
}

void setupWIFITime(){
  tft.setCursor(0, 30);
  tft.fillScreen(ST77XX_BLACK);
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(1);
  // Connect to Wi-Fi
  Serial.print("Connecting to ");
  tft.println("Connecting to ");
  tft.println(ssid);
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    tft.print(".");
  }
  Serial.println("");
  tft.println("");
  tft.println("WiFi connected.");
  Serial.println("WiFi connected.");

  // Init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  watchFace();

  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
}

static void turnDisplayON(void)
{
  if (!digitalRead(LITE)) {
    digitalWrite(LITE, HIGH);
  }
}

void setup(void) {
  Serial.begin(115200);

  delayStart = millis();

  pinMode(TOUCH, INPUT_PULLUP);
  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(BATTERY_M, INPUT);
  pinMode(LITE, OUTPUT);
  pinMode(VIBRATION, OUTPUT);

  // Set Switch State
  digitalWrite(LITE, HIGH);

  tft.init(240, 280);           // Init ST7789 240x240
  tft.setTextWrap(false);
  tft.fillScreen(ST77XX_BLACK);
  
  setupWIFITime();

  //----------------------------------------------------------------------------
  // For reciving notification
  // Set device as a Wi-Fi Station 
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
  
}

void loop() {
  /* power off the display after 6s */
  if ((millis() - delayStart) >= DELAY_TIME) {
      digitalWrite(LITE, LOW);
      upState =1;
      downState=1;
  }

  touchState = digitalRead(TOUCH);
  Serial.println(touchState);

    
  if (!digitalRead(UP)) {
    Serial.println("UP_BTN");
    delayStart = millis();
    turnDisplayON();
    if (upState == 1){
      Serial.println("1");
      upState =0;
      watchFace();
      delay(500);
    } else{
      upState =1;
      digitalWrite(LITE, LOW);
      Serial.println("0");
      delay(500);
    }
  }

  if (!digitalRead(DOWN)) {
    Serial.println("UP_DOWN");
   delayStart = millis();
    turnDisplayON();
    if (downState == 1){
      Serial.println("1");
      downState =0;
      secondFace();
      delay(500);
    } else{
      downState =1;
      notificationFace();
      Serial.println("0");
      delay(500);
    }
  }
}

// written by
// rahmanshaber @rahmanshaber
